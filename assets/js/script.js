$( document ).ready(function() {
    
    /* ******************************** Attributes ******************************** */
    // width
    $("[data-width]").each(function(){
        var width = $(this).attr('data-width');
        $(this).css({
            width: width
        });
    });
    //height
    $("[data-height]").each(function(){
        var height = $(this).attr('data-height');
        $(this).css({
            height: height
        });
    });
    // Background Image
    $("[bg-img]").each(function(){
        var imgSrc = $(this).attr('bg-img');
        $(this).css({
            'background-image': 'url('+imgSrc+')' 
        });
    });
    
    
    
    
    
    /* ******************************** Classes ******************************** */
    // Cover Parent Image
    if($(".parent-cover").length){
        $(".parent-cover").each(function(){
            var imgSrc = $(this).attr('src');
            $(this).parent().css({
                'background': 'url('+imgSrc+') center center no-repeat',
                '-webkit-background-size': '100% ',
                '-moz-background-size': '100%',
                '-o-background-size': '100%',
                'background-size': '100%',
                '-webkit-background-size': 'cover',
                '-moz-background-size': 'cover',
                '-o-background-size': 'cover',
                'background-size': 'cover'});
            $(this).hide();
        });
    }
    
    
    
     /* ******************************** Plugins ******************************** */
    // Owl Carousel
    if($('.owl-carousel').length){ $('.owl-carousel').owlCarousel(); }
    // SlimScroll
    if($('.style-scroll').length){ $('.style-scroll').slimScroll(); }
    
});